package com.example.daggeragain.step_9

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.daggeragain.R
import com.example.daggeragain.step_9.car.Car
import com.example.daggeragain.step_9.dagger.DaggerActivityComponent
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var car1: Car

    @Inject
    lateinit var car2: Car

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val appComponent = (application as MyApplication).getAppComponent()

        val activityComponent = DaggerActivityComponent.builder()
            .appComponent(appComponent)
            .engineCapacity(100)
            .horsePower(200)
            .build()


        activityComponent.inject(this)
        car1.drive()
        car2.drive()

    }
}


/*note: by default @Singleton is only component wide singleton not application wide
*
* we need to explicity store the Carcomponent in application class to retrieve it from same component
* */