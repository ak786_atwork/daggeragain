package com.example.daggeragain.step_9

import android.app.Application
import com.example.daggeragain.step_9.dagger.AppComponent
import com.example.daggeragain.step_9.dagger.DaggerAppComponent

class MyApplication: Application() {

    private lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.create()
    }


    fun getAppComponent(): AppComponent {
        return appComponent
    }
}