package com.example.daggeragain.step_9.car

import android.util.Log
import javax.inject.Inject

@PerActivity
class Car {
    private val TAG = "MY_CAR"

    private var driver: Driver
    private var engine: Engine
    private var wheels: Wheels

    @Inject
    constructor(driver: Driver, engine: Engine, wheels: Wheels) {
        this.driver = driver
        this.wheels = wheels
        this.engine = engine
    }

    @Inject
    public fun enableRemote(remote: Remote) {
        remote.setListener(this)
    }

    public fun drive() {
        engine.start()
        Log.d(TAG, "$driver driving $this ...")
    }
}


/* if we have constructor injection then fields and methods are automatically injected
*
* order is : constructor - fields - method
*
* and method injection is only useful when we have to pass the object as dependency
* */