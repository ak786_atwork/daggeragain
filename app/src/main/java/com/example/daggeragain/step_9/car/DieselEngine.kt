package com.example.daggeragain.step_9.car

import android.util.Log
import javax.inject.Inject

class DieselEngine: Engine {

    private val TAG = "MY_CAR"

    private var horsePower: Int

    @Inject
    constructor(horsePower: Int) {
        this.horsePower = horsePower
    }

    override fun start() {
        Log.d(TAG, "diesel engine started... horse power : $horsePower")
    }
}