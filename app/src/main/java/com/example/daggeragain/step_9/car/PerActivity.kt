package com.example.daggeragain.step_9.car

import javax.inject.Scope


@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@Scope
annotation class PerActivity