package com.example.daggeragain.step_9.car

import android.util.Log
import javax.inject.Inject

class Remote {
    private val TAG = "MY_CAR"

    @Inject
    constructor()

    public fun  setListener(car: Car) {
        Log.d(TAG, "remote connected..")
    }
}