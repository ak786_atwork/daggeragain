package com.example.daggeragain.step_9.car

import android.util.Log

class Tires {

    // we don't own this class it comes from third party libraries, so we can't do injection,

    private val TAG = "MY_CAR"

    constructor()

    fun inflate() {
        Log.d(TAG, "tires inflated...")
    }


}