package com.example.daggeragain.step_9.car


class Wheels {

    // we don't own this class

    private var rims: Rims
    private var tires: Tires

    constructor(rims: Rims, tires: Tires) {
        this.rims = rims
        this.tires = tires
    }
}