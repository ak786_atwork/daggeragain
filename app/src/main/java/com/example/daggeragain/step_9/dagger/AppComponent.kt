package com.example.daggeragain.step_9.dagger

import com.example.daggeragain.step_9.car.Driver
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [DriverModule::class])
interface AppComponent {

    fun getDriver(): Driver


}