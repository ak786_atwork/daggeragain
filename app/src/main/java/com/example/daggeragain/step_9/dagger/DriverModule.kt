package com.example.daggeragain.step_9.dagger

import com.example.daggeragain.step_9.car.Driver
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
abstract class DriverModule {

    companion object {

        @Singleton
        @Provides
        fun provideDriver(): Driver {
            return Driver()
        }

    }

}