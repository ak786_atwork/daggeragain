package com.example.daggeragain.step_1

import android.util.Log
import com.example.daggeragain.step_2.Engine
import com.example.daggeragain.step_2.Wheels
import javax.inject.Inject

class Car {
    private val TAG = "CAR"
    private var engine: Engine
    private var wheels: Wheels

    @Inject
    constructor(engine: Engine, wheels: Wheels) {
        this.engine = engine
        this.wheels = wheels
    }

    public fun drive() {
        Log.d(TAG, "driving...")
    }
}