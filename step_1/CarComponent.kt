package com.example.daggeragain.step_1

import com.example.daggeragain.step_2.Car
import dagger.Component

@Component
interface CarComponent {

    fun getCar(): Car
}