package com.example.daggeragain.step_1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.daggeragain.R
import com.example.daggeragain.step_2.Car

class MainActivity : AppCompatActivity() {

    private lateinit var car : Car

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        car = DaggerCarComponent.create().getCar()

        car.drive()

    }
}
