package com.example.daggeragain.step_2

import android.util.Log
import com.example.daggeragain.step_3.Engine
import com.example.daggeragain.step_3.Wheels
import javax.inject.Inject

class Car {
    private val TAG = "CAR__"
    private var engine: Engine
    private var wheels: Wheels

    @Inject
    constructor(engine: Engine, wheels: Wheels) {
        this.engine = engine
        this.wheels = wheels
    }

    public fun drive() {
        Log.d(TAG, "driving...")
    }
}