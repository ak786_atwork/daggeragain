package com.example.daggeragain.step_2

import com.example.daggeragain.step_3.MainActivity
import dagger.Component

@Component
interface CarComponent {

    //field injection
    //we can't pass supertype here
    fun inject(mainActivity: MainActivity)

}