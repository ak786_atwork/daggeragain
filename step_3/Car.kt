package com.example.daggeragain.step_3

import android.util.Log
import com.example.daggeragain.step_4.Engine
import com.example.daggeragain.step_4.Remote
import com.example.daggeragain.step_4.Wheels
import javax.inject.Inject

class Car {
    private val TAG = "MY_CAR"

    @Inject
    lateinit var engine: Engine
    private var wheels: Wheels

    @Inject
    constructor(wheels: Wheels) {
        this.wheels = wheels
    }

    @Inject
    public fun enableRemote(remote: Remote) {
        remote.setListener(this)
    }

    public fun drive() {
        Log.d(TAG, "driving...")
    }
}


/* if we have constructor injection then fields and methods are automatically injected
*
* order is : constructor - fields - method
*
* and method injection is only useful when we have to pass the object as dependency
* */