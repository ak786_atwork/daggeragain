package com.example.daggeragain.step_3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.daggeragain.R
import com.example.daggeragain.step_4.Car
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var car : Car

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        DaggerCarComponent.create().inject(this)

        car.drive()

    }
}
