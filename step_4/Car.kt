package com.example.daggeragain.step_5

import android.util.Log
import javax.inject.Inject

class Car {
    private val TAG = "MY_CAR"

    private var engine: Engine
    private var wheels: Wheels

    @Inject
    constructor(engine: Engine, wheels: Wheels) {
        this.wheels = wheels
        this.engine = engine
    }

    @Inject
    public fun enableRemote(remote: Remote) {
        remote.setListener(this)
    }

    public fun drive() {
        Log.d(TAG, "driving...")
    }
}


/* if we have constructor injection then fields and methods are automatically injected
*
* order is : constructor - fields - method
*
* and method injection is only useful when we have to pass the object as dependency
* */