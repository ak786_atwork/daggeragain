package com.example.daggeragain.step_5

import dagger.Component

@Component(modules = [WheelsModule::class])
interface CarComponent {

    //field injection
    //we can't pass supertype here
    fun inject(mainActivity: MainActivity)

}