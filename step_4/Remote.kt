package com.example.daggeragain.step_5

import android.util.Log
import com.example.daggeragain.step_4.Car
import javax.inject.Inject

class Remote {
    private val TAG = "MY_CAR"

    @Inject
    constructor()

    public fun  setListener(car: Car) {
        Log.d(TAG, "remote connected..")
    }
}