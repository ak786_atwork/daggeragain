package com.example.daggeragain.step_5.car

import android.util.Log
import javax.inject.Inject

class DieselEngine: Engine {

    private val TAG = "MY_CAR"

    @Inject
    constructor()

    override fun start() {
        Log.d(TAG, "diesel engine started...")
    }
}