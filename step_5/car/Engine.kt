package com.example.daggeragain.step_5.car


interface Engine {

    fun start()

}