package com.example.daggeragain.step_5.car

import javax.inject.Inject

class Rims {

    //we can't to do injection, they come from third party

    constructor()

}