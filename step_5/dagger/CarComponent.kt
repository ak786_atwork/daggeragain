package com.example.daggeragain.step_5.dagger

import com.example.daggeragain.step_5.MainActivity
import dagger.Component

@Component(modules = [WheelsModule::class, DieselEngineModule::class])
interface CarComponent {

    //field injection
    //we can't pass supertype here
    fun inject(mainActivity: MainActivity)

}