package com.example.daggeragain.step_5.dagger

import com.example.daggeragain.step_5.car.DieselEngine
import com.example.daggeragain.step_5.car.Engine
import dagger.Binds
import dagger.Module

@Module
abstract class DieselEngineModule {

    @Binds
    abstract fun provideEngine(dieselEngine: DieselEngine): Engine
}


/*
* 1. it is more concise
* 2. Dagger won't instantiate this class and won't call provide method, instead it will just instantiate petrol engine  directly
* 3. Whenever we just wants to bind the implementation with interface  then we should use bind
* 4. you can't use normal provides method here as dagger won't create instance
* 5. you can have static provides method here
* */

