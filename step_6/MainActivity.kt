package com.example.daggeragain.step_6

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.daggeragain.R
import com.example.daggeragain.step_9.car.Car
import com.example.daggeragain.step_9.dagger.CarComponent
import com.example.daggeragain.step_9.dagger.DaggerCarComponent
import com.example.daggeragain.step_9.dagger.DieselEngineModule
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var car : Car

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val carComponent = DaggerCarComponent.builder().dieselEngineModule(DieselEngineModule(1000)).build()

        carComponent.inject(this)
        car.drive()

    }
}
