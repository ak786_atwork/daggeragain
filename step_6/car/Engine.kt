package com.example.daggeragain.step_9.car


interface Engine {

    fun start()

}