package com.example.daggeragain.step_9.car

import android.util.Log
import com.example.daggeragain.step_9.car.Engine
import javax.inject.Inject

class PetrolEngine: Engine {

    private val TAG = "MY_CAR"

    @Inject
    constructor()

    override fun start() {
        Log.d(TAG, "Petrol engine started...")
    }
}