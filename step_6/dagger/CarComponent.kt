package com.example.daggeragain.step_9.dagger

import com.example.daggeragain.step_6.MainActivity
import dagger.Component

@Component(modules = [WheelsModule::class, DieselEngineModule::class])
interface CarComponent {

    //field injection
    //we can't pass supertype here
    fun inject(mainActivity: MainActivity)

}