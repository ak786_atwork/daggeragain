package com.example.daggeragain.step_9.dagger

import com.example.daggeragain.step_9.car.Engine
import com.example.daggeragain.step_9.car.PetrolEngine
import dagger.Binds
import dagger.Module

@Module
abstract class PetrolEngineModule {

    @Binds
    abstract fun provideEngine(petrolEngine: PetrolEngine): Engine
}


/*
* 1. it is more concise
* 2. Dagger won't instantiate this class and won't call provide method, instead it will just instantiate petrol engine  directly
* 3. Whenever we just wants to bind the implementation with interface  then we should use bind
* 4. you can't use normal provides method here as dagger won't create instance
* 5. you can have static provides method here
* */


/*
optimizations

@Module
class PetrolEngineModule {

    @Provides
    fun provideEngine(petrolEngine: PetrolEngine): Engine {
        return petrolEngine
    }

}*/