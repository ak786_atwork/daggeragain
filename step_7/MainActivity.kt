package com.example.daggeragain.step_7

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.daggeragain.R
import com.example.daggeragain.step_9.car.Car
import com.example.daggeragain.step_9.dagger.DaggerCarComponent
import com.example.daggeragain.step_9.dagger.DieselEngineModule
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var car: Car

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val carComponent = DaggerCarComponent.builder()
            .horsePower(150)
            .engineCapacity(300)
            .build()

        carComponent.inject(this)
        car.drive()

    }
}
