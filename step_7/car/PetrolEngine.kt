package com.example.daggeragain.step_9.car

import android.util.Log
import javax.inject.Inject
import javax.inject.Named

class PetrolEngine: Engine {

    private val TAG = "MY_CAR"

    private var horsePower : Int
    private var engineCapacity : Int

    @Inject
    constructor(@Named("horse power") horsePower : Int, @Named("engine capacity") engineCapacity: Int) {
        this.horsePower = horsePower
        this.engineCapacity = engineCapacity
    }

    override fun start() {
        Log.d(TAG, "Petrol engine started...with capacity : $engineCapacity and  horsepower : $horsePower")
    }
}