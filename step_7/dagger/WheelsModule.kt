package com.example.daggeragain.step_9.dagger

import com.example.daggeragain.step_9.car.Rims
import com.example.daggeragain.step_9.car.Tires
import com.example.daggeragain.step_9.car.Wheels
import dagger.Module
import dagger.Provides

@Module
class WheelsModule {

    companion object {

        @Provides
        fun provideRims(): Rims {
            return Rims()
        }

        @Provides
        fun provideTires(): Tires {
            val tires = Tires()
            tires.inflate()
            return tires
        }


        @Provides
        fun provideWheels(rims: Rims, tires: Tires): Wheels {
            //we can use Builder pattern to add complex configuration to object

            return Wheels(rims, tires)
        }

    }
}


//above all methods doesn't depend upon instance state of module, so we can make them static to improve performance, so now dagger
//don't need to create object