package com.example.daggeragain.step_8

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.daggeragain.R
import com.example.daggeragain.step_9.car.Car
import com.example.daggeragain.step_9.dagger.DaggerCarComponent
import com.example.daggeragain.step_9.dagger.DieselEngineModule
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var car1: Car

    @Inject
    lateinit var car2: Car

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val carComponent = DaggerCarComponent.builder()
            .horsePower(150)
            .engineCapacity(300)
            .build()

        carComponent.inject(this)
        car1.drive()
        car2.drive()

    }
}

/**
 * Singleton in Dagger just provide component wide singleton not application wide.
 * */
