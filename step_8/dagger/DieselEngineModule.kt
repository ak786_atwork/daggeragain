package com.example.daggeragain.step_9.dagger

import com.example.daggeragain.step_9.car.DieselEngine
import com.example.daggeragain.step_9.car.Engine
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Inject

@Module
class DieselEngineModule {

    private var horsePower: Int

    constructor(horsePower: Int) {
        this.horsePower = horsePower
    }

    @Provides
    fun provideHorsePower(): Int {
        return horsePower
    }

    @Provides
    fun provideEngine(dieselEngine: DieselEngine): Engine {
        return dieselEngine
    }
}


/*
* 1. it is more concise
* 2. Dagger won't instantiate this class and won't call provide method, instead it will just instantiate petrol engine  directly
* 3. Whenever we just wants to bind the implementation with interface  then we should use bind
* 4. you can't use normal provides method here as dagger won't create instance
* 5. you can have static provides method here
*
*
* limitations
* 1. Binds can't be used when we want to pass arguments at runtime
*
* */

